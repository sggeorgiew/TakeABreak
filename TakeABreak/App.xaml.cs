﻿using Hardcodet.Wpf.TaskbarNotification;
using System.Windows;

namespace TakeABreak
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TaskbarIcon notifyIcon;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
            notifyIcon.ShowBalloonTip("TakeABreak", "TakeABreak is minimized to system tray. It will track your time in front of the monitor and signal for breaks. ", BalloonIcon.Info);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose();
            base.OnExit(e);
        }
    }
}
