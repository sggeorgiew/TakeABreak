﻿using System;

namespace TakeABreak.Tools
{
    public static class MillisecondsConverter
    {
        public static int Convert(int minutes)
        {
            return (int)TimeSpan.FromMinutes(minutes).TotalMilliseconds;
        }

        public static int ConvertBack(int milliseconds)
        {
            return TimeSpan.FromMilliseconds(milliseconds).Minutes;
        }
    }
}
