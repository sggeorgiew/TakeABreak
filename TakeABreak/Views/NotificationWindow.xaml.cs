﻿using System.Windows;

namespace TakeABreak.Views
{
    /// <summary>
    /// Interaction logic for NotificationWindow.xaml
    /// </summary>
    public partial class NotificationWindow
    {
        public NotificationWindow()
        {
            InitializeComponent();
        }

        private void TakenButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
