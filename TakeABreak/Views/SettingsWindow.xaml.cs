﻿using System;
using System.Windows;
using TakeABreak.ViewModels;

namespace TakeABreak.Views
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        private SettingsViewModel model;

        public SettingsWindow()
        {
            model = new SettingsViewModel(Config.Load());

            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            SettingsGrid.DataContext = model;
            base.OnInitialized(e);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Config.Save(model.GetConfig());
            DialogResult = true;
            Close();
        }
    }
}
