﻿using Newtonsoft.Json;
using System.IO;

namespace TakeABreak
{
    /// <summary>
    /// Application configuration
    /// </summary>
    public class Config
    {
        /// <summary>
        /// Path to the configuration file
        /// </summary>
        private const string configFile = "./config.json";

        /// <summary>
        /// Gets or sets the minutes for break
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Gets or sets whether the application will be started as tray
        /// </summary>
        public bool StartMinimized { get; set; }

        /// <summary>
        /// Gets or sets whether to count the taken breaks or not
        /// </summary>
        public bool CountBreaks { get; set; }

        public Config()
        {
            Minutes = 90;
            StartMinimized = true;
            CountBreaks = true;
        }

        /// <summary>
        /// Saves the configuration in file
        /// </summary>
        /// <param name="settings"></param>
        public static void Save(Config settings)
        {
            using (FileStream fs = new FileStream(configFile, FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(settings));
                }
            }
        }

        /// <summary>
        /// Loads the configuration from file
        /// </summary>
        /// <returns></returns>
        public static Config Load()
        {
            string json;

            using (FileStream fs = new FileStream(configFile, FileMode.OpenOrCreate))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    json = sr.ReadToEnd();
                }
            }

            var settings = JsonConvert.DeserializeObject<Config>(json);
            if (settings != null)
            {
                return settings;
            }

            return new Config();
        }
    }
}
