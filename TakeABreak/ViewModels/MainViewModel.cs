﻿using System.Collections.Generic;
using System.ComponentModel;

namespace TakeABreak.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string information;
        private string status;
        private bool isStarted;

        /// <summary>
        /// Gets or sets the information
        /// </summary>
        public string Information
        {
            get => information;
            set => SetAndNotify(ref information, value, nameof(Information));
        }

        /// <summary>
        /// Gets or sets the current status
        /// </summary>
        public string Status
        {
            get => status;
            set => SetAndNotify(ref status, value, nameof(Status));
        }

        /// <summary>
        /// Gets or sets whether the application timer is running or not
        /// </summary>
        public bool IsStarted
        {
            get => isStarted;
            set => SetAndNotify(ref isStarted, value, nameof(IsStarted));
        }

        /// <summary>
        /// Gets or sets the total breaks taken
        /// </summary>
        public int TotalBreaks { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetAndNotify<T>(ref T field, T value, string propertyName)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                RaisePropertyChanged(propertyName);
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
