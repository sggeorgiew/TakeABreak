﻿namespace TakeABreak.ViewModels
{
    public class SettingsViewModel
    {
        /// <summary>
        /// Gets or sets the minutes for break
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Gets or sets whether the application will be started as tray
        /// </summary>
        public bool StartMinimized { get; set; }

        /// <summary>
        /// Gets or sets whether to count the taken breaks or not
        /// </summary>
        public bool CountBreaks { get; set; }

        public SettingsViewModel()
        { }

        public SettingsViewModel(Config settings)
        {
            Minutes = settings.Minutes;
            StartMinimized = settings.StartMinimized;
            CountBreaks = settings.CountBreaks;
        }

        /// <summary>
        /// Gets the current config settings
        /// </summary>
        public Config GetConfig()
        {
            var settings = new Config
            {
                Minutes = Minutes,
                StartMinimized = StartMinimized,
                CountBreaks = CountBreaks
            };

            return settings;
        }
    }
}
